import { IsNotEmpty } from 'class-validator';

export class TodoDto {

    @IsNotEmpty()
    id: number;

    @IsNotEmpty()
    tarea: string;

    @IsNotEmpty()
    estado: boolean;
    
}