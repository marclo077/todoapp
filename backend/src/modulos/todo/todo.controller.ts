import { TodoService } from './todo.service';
import { Controller, Get, Post, Body, Delete, Param, ParseIntPipe, Put, Patch, ParseBoolPipe } from '@nestjs/common';
import { Todo } from './todo.entity';

@Controller('tasks')
export class TodoController {

    constructor(private readonly _todoService: TodoService){}

    @Get()
    async getTareas(): Promise<Todo[]> {
        const tasks = await this._todoService.getAll();
        return tasks;
    }

    @Get(':id')
    async getTarea(@Param('id', ParseIntPipe) id: number): Promise<Todo> {
        const task = await this._todoService.getOne(id);
        return task;
    }

    @Post()
    async storeTarea(@Body() tarea: Todo): Promise<Todo> {
        const task = await this._todoService.create(tarea);
        return task;
    }

    @Put(':id') //@Param('id', ParseIntPipe) saca la propiedad id le objeto que llegue para parsearlo a entero
    async changeStatus(@Param('id', ParseIntPipe) id: number): Promise<void> {
        return await this._todoService.changeStatus(id);
    }

    @Patch(':id')
    async updateTask(@Param('id', ParseIntPipe) id: number, @Body() tarea: Todo){
        return await this._todoService.update(id, tarea);
    }

    @Delete(':id') //@Param('id', ParseIntPipe) saca la propiedad id le objeto que llegue para parsearlo a entero
    async deleteTask(@Param('id', ParseIntPipe) id: number){
        return await this._todoService.delete(id);
    }
}
